package main

import (
  "flag"
  "fmt"
  "time"

  "k8s.io/client-go/kubernetes"
  "k8s.io/client-go/pkg/api/v1"
  "k8s.io/client-go/tools/clientcmd"
)

var (
  kubeconfig = flag.String("kubeconfig", "./etc/minikube.config", "absolute path to the kubeconfig file")
)

func main() {
  flag.Parse()
  // uses the current context in kubeconfig
  config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
  if err != nil {
    panic(err.Error())
  }
  // creates the clientset
  clientset, err := kubernetes.NewForConfig(config)
  if err != nil {
    panic(err.Error())
  }
  for {
    pods, err := clientset.Core().Pods("").List(v1.ListOptions{})
    if err != nil {
      panic(err.Error())
    }
    fmt.Printf("There are %d pods in the cluster\n", len(pods.Items))
    time.Sleep(10 * time.Second)
  }
}
