package k8s

import (
  "k8s.io/client-go/pkg/api/v1"
	"encoding/json"
)

func (k *Kube) GetDeployInfo(namespace string) (b []byte, err error) {
	pods, err := k.KubeConfigSet.Core().Pods(namespace).List(v1.ListOptions{})
	if err != nil {
		return b, err
	}
	return json.Marshal(pods.Items)
}
