package k8s

import "testing"
import "github.com/stretchr/testify/assert"

func TestGetDeploy(t *testing.T) {
  var kubeconfig = "../etc/minikube.conf"
  kube, err := Set(kubeconfig)
  assert.Nil(t, err, "Unexpected kube set operation")
  _, err = kube.GetDeployInfo("default")
  assert.Nil(t, err, "Unexpected kube get deploy operation")
}
