package k8s

import "math/rand"

func RandStringBytesRmndr(n int) string {
  const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
  b := make([]byte, n)
  for i := range b {
      b[i] = letterBytes[rand.Int63() % int64(len(letterBytes))]
  }
  return string(b)

}
