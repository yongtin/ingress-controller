package k8s

import (
  "k8s.io/client-go/pkg/api/v1"
	"encoding/json"
  "fmt"
)

func (k *Kube) GetIngress(namespace string) (b []byte, err error) {
	ingresses, err := k.KubeConfigSet.ExtensionsV1beta1().Ingresses(namespace).List(v1.ListOptions{})
	if err != nil {
		return b, err
	}
	return json.Marshal(ingresses.Items)
}

func (k *Kube) PrintIngress(namespace string) (err error) {
	ingresses, err := k.KubeConfigSet.ExtensionsV1beta1().Ingresses(namespace).List(v1.ListOptions{})
	if err != nil {
		return err
	}
  for _, ingress := range ingresses.Items {
    // ingress.Name
    for _, rule := range ingress.Spec.Rules {
      // rule.Host
      for _, _ = range rule.HTTP.Paths {
        /*var ListenAddress = "*"
        var ListenPort = "80"
        var BackendMode = "http"
        var LoadBalanceAlgorithm = "roundrobin"
        var HealthCheckURL = "/status"
        var BackendName = rule.Host
        var HostHeader = path.Backend.ServiceName
        var ServerPort = path.Backend.ServicePort.IntValue()
        var ServerName =
        var ServerAddr =
        var ServerOption = "check weight 10 inter 20s fastinter 10s rise 2 fall 2 slowstart 60s maxconn 120"
        var s = fmt.Sprintf("BackendName:%s,  HostHeader:%s,,ServerPort:%d")*/
        fmt.Println("")
      }
    }
  }
	return nil
}
