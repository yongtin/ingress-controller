package k8s

import "testing"
import "github.com/stretchr/testify/assert"
//import "fmt"

func TestGetIngresses(t *testing.T) {
  var kubeconfig = "../etc/minikube.conf"
  kube, err := Set(kubeconfig)
  assert.Nil(t, err, "Unexpected kube set operation")
  //var dat []byte
  _, err = kube.GetIngress("default")
  //fmt.Println(string(dat))
  assert.Nil(t, err, "Unexpected kube get deploy operation")
}

func TestIngresses(t *testing.T) {
  var kubeconfig = "../etc/minikube.conf"
  kube, err := Set(kubeconfig)
  assert.Nil(t, err, "Unexpected kube set operation")
  kube.PrintIngress("default")
}
