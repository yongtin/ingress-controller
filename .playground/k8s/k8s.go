package k8s

import (
	"flag"
  "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)
type Kube struct {
  KubeConfigFilename string
  KubeConfigSet *kubernetes.Clientset
}

func Set(c string) (k Kube, err error) {
	var kubeconfig = flag.String(RandStringBytesRmndr(10), c, "absolute path to the kubeconfig ")
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		return k, err
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return k, err
	}
	k = Kube{
		KubeConfigFilename: c,
		KubeConfigSet: clientset,
	}
	return k, nil
}
