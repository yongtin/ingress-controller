package k8s

import "testing"
import "github.com/stretchr/testify/assert"

func TestMissingConfig(t *testing.T) {
  var kubeconfig = "badfile"
  _, err := Set(kubeconfig)
  assert.NotNil(t, err, "Unexpected kube set operation")
  /*err = kube.GetDeployInfo()
  assert.Nil(t, err, "Unexpected kube get deploy operation")*/
}

