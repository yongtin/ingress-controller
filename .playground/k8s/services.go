package k8s

import (
  "k8s.io/client-go/pkg/api/v1"
	"encoding/json"
	"fmt"
)

func (k *Kube) ListServices(namespace string) (b []byte, err error) {
	services, err := k.KubeConfigSet.Core().Services(namespace).List(v1.ListOptions{})
	if err != nil {
		return b, err
	}
	return json.Marshal(services.Items)
}

func (k *Kube) GetServices(namespace string, serviceName string) (b []byte, err error) {
	service, err := k.KubeConfigSet.Core().Services(namespace).Get(serviceName)
	if err != nil {
		return b, nil
	}
	fmt.Println(fmt.Sprintf("IP: %s", service.Spec.ClusterIP))
	fmt.Println(fmt.Sprintf("ClusterName: %s", service.ClusterName))
	return json.Marshal(service)
}
