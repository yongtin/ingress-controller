package k8s

import "testing"
import "github.com/stretchr/testify/assert"
import "fmt"

func TestListServices(t *testing.T) {
  var kubeconfig = "../etc/minikube.conf"
  kube, err := Set(kubeconfig)
  assert.Nil(t, err, "Unexpected kube set operation")
  //var dat []byte
  _, err = kube.ListServices("default")
  //fmt.Println(string(dat))
  assert.Nil(t, err, "Unexpected kube get deploy operation")
}

func TestGetServices(t *testing.T) {
  var kubeconfig = "../etc/minikube.conf"
  kube, err := Set(kubeconfig)
  assert.Nil(t, err, "Unexpected kube set operation")
  var dat []byte
  dat, err = kube.GetServices("default", "example-nginx-service")
  fmt.Println(string(dat))
  assert.Nil(t, err, "Unexpected kube get deploy operation")
}
