FROM haproxy:1.7
ADD ./etc/haproxy /etc/haproxy
ADD ./etc/kubernetes /etc/kubernetes
ADD .output/guideRunner /opt/bin/guideRunner
EXPOSE 80 1936

CMD [ "/opt/bin/guideRunner", "--configdir", "/etc" ]
