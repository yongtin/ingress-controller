#
# Makefile for building guiderunner
#
PWD := $(shell pwd)
OUTPUT_DIR := ".output"
DOCKER_ID := $(shell docker run -v $(PWD)/.output:/go/bin -v $(PWD):/go/src/gitlab.com/yongtin/ingress-controller -td golang /bin/bash)

gobuild: dockerhealth
	@echo Docker ID: $(DOCKER_ID)
	docker exec -t $(DOCKER_ID) go get github.com/inconshreveable/log15
	docker exec -t $(DOCKER_ID) go get golang.org/x/crypto/ssh
	docker exec -t $(DOCKER_ID) go get github.com/stretchr/testify/assert
	docker exec -t $(DOCKER_ID) go get gopkg.in/urfave/cli.v1
	docker exec -t $(DOCKER_ID) go get k8s.io/client-go/kubernetes
	docker exec -t $(DOCKER_ID) bash -c "cd /go/src/gitlab.com/yongtin/ingress-controller; go build -o /go/bin/guideRunner"

goclean: dockerhealth
	docker rm -f $(DOCKER_ID)

imagebuild: dockerhealth
	docker build -t registry.gitlab.com/yongtin/ingress-controller .

imagepush: dockerhealth
	docker push registry.gitlab.com/yongtin/ingress-controller

clean: dockerhealth
	@mkdir -p $(PWD)/.output
	@rm -rf $(PWD)/.output/*

dockerhealth:
	@docker ps -q > /dev/null

build: dockerhealth clean gobuild goclean imagebuild

