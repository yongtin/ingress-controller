guiderunner
===================
----------


An ingress-controller for kubernetes
-------------

guiderunner is a proof of concept ingress controller for kubernetes based on haproxy. At the moment, only host-based kubernetes ingress rules are being examined and processed.

Quickstart
-------------
####Installing sample app and sample ingress rules
```
# installing sample app (e.g. nginx and jenkins)
$ kubectl create -f ./k8s-examples/simple-apps.yml

# installing sample ingress rules
$ kubectl create -f ./k8s-examples/simple-ingress-rules.yml

```

####Deploy kubernetes config and haproxy templates using configmap
sample kubernetes minikube config:
```
apiVersion: v1
clusters:
- cluster:
    certificate-authority: /Users/yongtin/.minikube/ca.crt
    server: https://192.168.99.103:8443
  name: minikube
contexts:
- context:
    cluster: minikube
    user: minikube
  name: minikube
current-context: minikube
kind: Config
preferences: {}
users:
- name: minikube
  user:
    client-certificate: /Users/yongtin/.minikube/apiserver.crt
    client-key: /Users/yongtin/.minikube/apiserver.key
```

sample haproxy templates (haproxy.cfg.tmpl)
```
global
  chroot /etc/haproxy
  user root
  group root
  daemon
  maxconn {{ .MaxConn }}
  spread-checks 5
  max-spread-checks 15000
  tune.ssl.default-dh-param 2048
#  stats socket /haproxy/socket
#  server-state-file global
#  server-state-base /haproxy/state
#  lua-load /etc/haproxy/lua/getpids.lua
#  lua-load /etc/haproxy/lua/getconfig.lua
#  lua-load /etc/haproxy/lua/getmaps.lua
#  lua-load /etc/haproxy/lua/signalhap.lua

defaults
  load-server-state-from-file global
  log global
  option dontlognull
  option http-server-close
  option redispatch
  retries                   3
  backlog               10000
  maxconn               10000
  timeout connect          3s
  timeout client          30s
  timeout server          30s
  timeout tunnel        3600s
  timeout http-keep-alive  1s
  timeout http-request    15s
  timeout queue           30s
  timeout tarpit          60s
#  errorfile 400 /etc/haproxy/errors/400.http
#  errorfile 403 /etc/haproxy/errors/403.http
#  errorfile 408 /etc/haproxy/errors/408.http
#  errorfile 500 /etc/haproxy/errors/500.http
#  errorfile 502 /etc/haproxy/errors/502.http
#  errorfile 503 /etc/haproxy/errors/503.http
#  errorfile 504 /etc/haproxy/errors/504.http

listen stats
  bind {{ .StatsBindAddress }}:{{ .StatsBindPort }}
  balance
  mode {{ .StatsMode }}
  stats enable
  stats uri {{ .StatsURI }}
  stats refresh 5s
  monitor-uri {{ .StatsMonitorURI }}
  acl getpid path {{ .StatsGetPIDPath }}
  acl getvhostmap path {{ .StatsGetVHostMapPath }}
  acl getappmap path {{ .StatsGetAppMapPath }}
  acl getconfig path {{ .StatsGetConfigPath }}
#  http-request use-service lua.getpids if getpid
#  http-request use-service lua.getvhostmap if getvhostmap
#  http-request use-service lua.getappmap if getappmap
#  http-request use-service lua.getconfig if getconfig

frontend ft_hostbased_allapps
  bind {{ .VHostMapFrontEndListenAddress }}:{{ .VHostMapFrontEndListenPort }}
  mode {{ .VHostMapMode }}
  use_backend %[req.hdr(host),lower,regsub(:.*$,,),map(/etc/haproxy/domain2backend.map)]

{{ range $index, $backend := .Backends }}
backend {{ $backend.ServerName }}
  mode {{ $backend.BackendMode }}
  balance {{ $backend.LoadBalanceAlgorithm }}
  option forwardfor
  http-request set-header X-Forwarded-Port %[dst_port]
  http-request add-header X-Forwarded-Proto https if { ssl_fc }
  option httpchk HEAD {{ $backend.HealthCheckURL }} HTTP/1.1\r\nHost:\ {{ $backend.HostHeader }}
  timeout check 30s
  server {{ $backend.ServerName }} {{ $backend.ServerAddr }}:{{ $backend.ServerPort }} {{ $backend.ServerOption }}

{{ end }}

```

sample haproxy template domain2backend.map.tmpl map file
```
{{ range $index, $backend := .Backends }}
{{ $backend.HostHeader }} {{ $backend.ServerName }}
{{ end }}
```

####Deploy ingress-controller app and service in kubernetes
```
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: guiderunner-deployment
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: guiderunner-deployment
    spec:
      volumes:
      - name: kubernetes-config
        configMap:
          name: guiderunner-config
          items:
          - key: kubernetes.config
            path: config
      - name: haproxy-template
        configMap:
          name: guiderunner-config
          items:
          - key: haproxy.cfg.tmpl
            path: haproxy.cfg.tmpl
          - key: domain2backend.map.tmpl
            path: domain2backend.map.tmpl
      containers:
      - name: guiderunner
        image: registry.gitlab.com/yongtin/ingress-controller
        volumeMounts:
        - mountPath: /etc/kubernetes
          name: kubernetes-config
        - mountPath: /etc/haproxy
          name: haproxy-template
        ports:
        - containerPort: 80
          name: web
          protocol: TCP
        - containerPort: 1936
          name: admin
          protocol: TCP
---
kind: Service
apiVersion: v1
metadata:
  name: guiderunner-service
spec:
  externalIPs:
  - 10.0.0.1
  #clusterIP: ""
  #loadBalancerIP: ""
  #type: LoadBalancer
  selector:
    app: guiderunner-deployment
  ports:
    - port: 8880
      targetPort: 80
      protocol: TCP
      name: guiderunner-web-service
    - port: 1936
      targetPort: 1936
      protocol: TCP
      name: guiderunner-admin-service
```

####Test it out
```
$ curl -H "Host: foo.bar.com" http://10.0.0.1:8880
<html>
  This is a testing ee-nginx-app for ee-autoscale
</html>
```

Features
-------------
* host-based ingress routing
* template based haproxy config
* plugins design to allow supports for more LB backends



High Level Overview
-------------

kubernetes supports a data object called ingress rules. This allows applications and services to define how rules can be routed into services and pods.

guiderunner docker image contains the two components to "monitor" the ingress rules.

* guiderunner (golang binary)
* haproxy

####What it does:

* At guiderunner boot time it checks for haproxy template and kubernetes config
* Fetch the ingress rules that exists in the kubernetes environment.
* Based on the ingress rules, process the rules and stored them into data structure.
* Using the haproxy template provided, guiderunner replaces the data fetched from ingress rules previously with real values.
* Finally, reload haproxy
