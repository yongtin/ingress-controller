package controller

import "gitlab.com/yongtin/ingress-controller/route"

type Controller interface{
  New(config string) (_ error)
  Publish(config string) (_ route.Route)
}

type ControllerCreator func() Controller

var RegisteredController = map[string]ControllerCreator{}

func Add(controllerName string, controllerCreator ControllerCreator){
  RegisteredController[controllerName] = controllerCreator
}
