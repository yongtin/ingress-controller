package k8sinc

import (
  "math/rand"
  "flag"
  "fmt"
  "k8s.io/client-go/kubernetes"
  "k8s.io/client-go/tools/clientcmd"
  "k8s.io/client-go/pkg/api/v1"
  meta_v1 "k8s.io/client-go/pkg/apis/meta/v1"
  "gitlab.com/yongtin/ingress-controller/controller"
  "gitlab.com/yongtin/ingress-controller/route"
)

type K8SIngressController struct {
  ProviderName string
  KubeConfigFile string
  KubeConfigSet *kubernetes.Clientset
}

func RandStringBytesRmndr(n int) string {
  const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
  b := make([]byte, n)
  for i := range b {
      b[i] = letterBytes[rand.Int63() % int64(len(letterBytes))]
  }
  return string(b)
}

func (k *K8SIngressController) New(config string) error {
  var err error
  var kcfg = flag.String(RandStringBytesRmndr(10), config, "kubeconfig")

  k.KubeConfigFile = config
  c, err := clientcmd.BuildConfigFromFlags("", *kcfg)
  if err != nil {
    return err
  }
  k.KubeConfigSet, err = kubernetes.NewForConfig(c)
  if err != nil { return err }
  return nil
}

func (k *K8SIngressController) Publish(namespace string) (r route.Route) {
  ingresses, err := k.KubeConfigSet.ExtensionsV1beta1().Ingresses(namespace).List(v1.ListOptions{})
  if err != nil {
    r.Error = err
    return r
  }
  for _, ingress := range ingresses.Items {
    for _, rule := range ingress.Spec.Rules {
      for _, path := range rule.HTTP.Paths {
        clusterIP, err := k.GetServiceClusterIP(namespace, path.Backend.ServiceName)
        if err != nil {
          r.Error = err
          return r
        }
        b := route.Backend{
          BackendName: ingress.Name,
          ListenAddress: "*",
          ListenPort: 80,
          BackendMode: "http",
          LoadBalanceAlgorithm: "roundrobin",
          HealthCheckURL: "/status",
          HostHeader: rule.Host,
          ServerName: fmt.Sprintf("%s-%d",path.Backend.ServiceName,path.Backend.ServicePort.IntValue()),
          ServerAddr: clusterIP,
          ServerPort: path.Backend.ServicePort.IntValue(),
          ServerOption: "check weight 10 inter 20s fastinter 10s rise 2 fall 2 slowstart 60s maxconn 120",
        }
        r.Backends = append(r.Backends, b)
      }
    }
  }
  r.Error = nil
  return r
}

func (k *K8SIngressController) GetServiceClusterIP(namespace string, serviceName string) (ip string, err error) {
  service, err := k.KubeConfigSet.Core().Services(namespace).Get(serviceName, meta_v1.GetOptions{})
  if err != nil {
    return ip, err
  }
  return service.Spec.ClusterIP, nil
}

/* where the controller gets initialized */
func init() {
  controller.Add("k8s", func() controller.Controller {
    return &K8SIngressController{
      ProviderName: "k8s",
    }
  })
}
