package k8sinc

import (
  "gitlab.com/yongtin/ingress-controller/controller"
  "testing"
  "github.com/stretchr/testify/assert"
)

func TestK8Sinc(t *testing.T) {
  controller.Add("k8s", func() controller.Controller {
    return &K8SIngressController{
      ProviderName: "k8s",
    }
  })

  controllerCreator, ok := controller.RegisteredController["k8s"]
  assert.True(t, ok)
  var c = controllerCreator()
  c.New("../../etc/minikube.conf")
  var r = c.Publish("default")
  assert.Nil(t, r.Error)
}
