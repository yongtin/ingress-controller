package haproxy

import (
  "os"
  "path"
  "strings"
  "text/template"
  "gitlab.com/yongtin/ingress-controller/loadbalancer"
  "gitlab.com/yongtin/ingress-controller/route"
)

type HAproxyLB struct {
  ProviderName string
  MainConfig MainHAproxyConfig
  TemplateConfigPath string
  MapFilePaths []string
}

type MainHAproxyConfig struct {
  MaxConn int
  StatsBindAddress string
  StatsBindPort int
  StatsURI string
  StatsMonitorURI string
  StatsGetPIDPath string
  StatsGetVHostMapPath string
  StatsGetAppMapPath string
  StatsGetConfigPath string
  StatsMode string
  VHostMapFrontEndListenAddress string
  VHostMapFrontEndListenPort int
  VHostMapMode string
  AppMapFrontEndListenAddress string
  AppMapFrontEndListenPort int
  Backends []route.Backend
}

func (h *HAproxyLB) New(mainConfigPath string, mapFilePaths ...string) error {
  h.MainConfig = MainHAproxyConfig{
    MaxConn: 50000,
    StatsBindAddress: "0.0.0.0",
    StatsBindPort: 1936,
    StatsURI: "/admin",
    StatsMonitorURI: "/admin_haproxy_health_check",
    StatsGetPIDPath: "/admin_haproxy_getpids",
    StatsGetVHostMapPath: "/admin_haproxy_getvhostmap",
    StatsGetAppMapPath: "/admin_haproxy_getappmap",
    StatsGetConfigPath: "/admin_haproxy_getconfig",
    StatsMode: "http",
    VHostMapFrontEndListenAddress: "*",
    VHostMapFrontEndListenPort: 80,
    VHostMapMode: "http",
    AppMapFrontEndListenAddress: "*",
    AppMapFrontEndListenPort: 8080,
  }
  h.TemplateConfigPath = mainConfigPath
  if mapFilePaths != nil { h.MapFilePaths = mapFilePaths }
  return nil
}

func (h *HAproxyLB) Subscribe(r route.Route) error {
  if r.Error != nil {
    return r.Error
  }
  h.MainConfig.Backends = r.Backends
  var err = h.replaceTemplateWithConfig(h.TemplateConfigPath)
  if err != nil { return err }

  for _, mapFilePath := range h.MapFilePaths {
    err = h.replaceTemplateWithConfig(mapFilePath)
    if err != nil { return err }
  }
  return nil
}

func (h *HAproxyLB) replaceTemplateWithConfig(templateFile string) error {
  tmpl, err := template.New(path.Base(templateFile)).ParseFiles(templateFile)
  if err != nil { return err }
  _, err = os.Stat(strings.TrimSuffix(templateFile, ".tmpl"))
  if os.IsNotExist(err) {
    var file, fileerr = os.Create(strings.TrimSuffix(templateFile, ".tmpl"))
    if fileerr != nil { return fileerr }
    defer file.Close()
  }
  realFile, err := os.OpenFile(strings.TrimSuffix(templateFile, ".tmpl"), os.O_WRONLY, 0644)
  if err != nil { return err }
  err = tmpl.Execute(realFile, h.MainConfig)
  if err != nil { return err }
  return realFile.Close()
}

/* where the loadbalancer gets initialized */
func init() {
  loadbalancer.Add("haproxy", func() loadbalancer.LoadBalancer {
    return &HAproxyLB{
      ProviderName: "haproxy",
    }
  })
}
