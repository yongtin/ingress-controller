package haproxy

import (
  "gitlab.com/yongtin/ingress-controller/loadbalancer"
  "gitlab.com/yongtin/ingress-controller/route"
  "testing"
  "github.com/stretchr/testify/assert"
)

func TestHAproxyLB(t *testing.T) {
  loadbalancer.Add("haproxy", func() loadbalancer.LoadBalancer {
    return &HAproxyLB{
      ProviderName: "haproxy",
    }
  })
  loadbalancerCreator, ok := loadbalancer.RegisteredLoadBalancer["haproxy"]
  assert.True(t, ok)
  var lb = loadbalancerCreator()
  lb.New("../../etc/haproxy.cfg.tmpl","../../etc/domain2backend.map.tmpl")
  r := make(chan route.Route)
  go func() {
    r <- route.Route{
      Backends: []route.Backend{
        {
          BackendName: "backend-nginx",
          ListenAddress: "*",
          ListenPort: 80,
          BackendMode: "http",
          LoadBalanceAlgorithm: "round-robin",
          HealthCheckURL: "/status",
          HostHeader: "test-tyong.example.com",
          ServerName: "backend-nginx",
          ServerAddr: "172.16.100.100",
          ServerPort: 10000,
          ServerOption: "check weight 10 inter 20s fastinter 10s rise 2 fall 2 slowstart 60s maxconn 120",
        },
      },

    }
  }()
  var err error
  err = lb.Subscribe(<-r)
  assert.Nil(t, err)
}
