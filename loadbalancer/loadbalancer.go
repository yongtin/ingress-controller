package loadbalancer

import "gitlab.com/yongtin/ingress-controller/route"

type LoadBalancer interface{
  New(config string, args ...string) (_ error)
  Subscribe(route.Route) (_ error)
}

type LoadBalancerCreator func() LoadBalancer

var RegisteredLoadBalancer = map[string]LoadBalancerCreator{}

func Add(loadBalancerType string, loadBalancerCreator LoadBalancerCreator){
  RegisteredLoadBalancer[loadBalancerType] = loadBalancerCreator
}
