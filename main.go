package main

import (
  "fmt"
  "sort"
  "os"
  "time"
  "gitlab.com/yongtin/ingress-controller/controller"
  "gitlab.com/yongtin/ingress-controller/loadbalancer"
  _ "gitlab.com/yongtin/ingress-controller/controller/all"
  _ "gitlab.com/yongtin/ingress-controller/loadbalancer/all"
  "gitlab.com/yongtin/ingress-controller/route"
  "gitlab.com/yongtin/ingress-controller/shellpipe"
  cli "gopkg.in/urfave/cli.v1"
  log "github.com/inconshreveable/log15"
)


func main() {
  var controllerProvider string
  var controllerNameSpace string
  var lbProvider string
  var controllerConfig string
  var lbTemplatePath string
  var lbMapPath string
  var ready bool

  /* do some flags parsing */
  app := cli.NewApp()
  app.Name = "GuideRunner"
  app.Usage = "Controller that guides ingress traffic into the proper application"
  app.Version = "0.1.0"
  app.Flags = []cli.Flag {
    cli.StringFlag {
      Name: "c, controller",
      Usage: "name of the controller provider",
      Value: "k8s",
      EnvVar: "GR_CONTROLLER_PROVIDER",
    },
    cli.StringFlag {
      Name: "namespace",
      Usage: "controller namespace. only applies to k8s. default: 'default'",
      Value: "default",
      EnvVar: "GR_CONTROLLER_NAMESPACE",
    },
    cli.StringFlag {
      Name: "configdir",
      Usage: "controller config directory",
      Value: "etc",
      EnvVar: "GR_CONFIG_DIR",
    },
    cli.StringFlag {
      Name: "l, loadbalancer",
      Usage: "name of the loadbalancer provider",
      Value: "haproxy",
      EnvVar: "GR_LOADBALANCER_PROVIDER",
    },
  }
  app.Action = func(c *cli.Context) (err error) {
    controllerProvider = c.String("controller")
    controllerNameSpace = c.String("namespace")
    lbProvider = c.String("loadbalancer")
    controllerConfig = fmt.Sprintf("%s/kubernetes/config",c.String("configdir"))
    lbTemplatePath = fmt.Sprintf("%s/haproxy/haproxy.cfg.tmpl",c.String("configdir"))
    lbMapPath = fmt.Sprintf("%s/haproxy/domain2backend.map.tmpl",c.String("configdir"))

    _, err = os.Stat(controllerConfig)
    if os.IsNotExist(err) { return err }
    _, err = os.Stat(lbTemplatePath)
    if os.IsNotExist(err) { return err }
    _, err = os.Stat(lbMapPath)
    if os.IsNotExist(err) { return err }

    ready = true
    return nil
  }
  //app.OnUsageError = func(c *cli.Context, err error, isSubcommand bool) error { return err }
  sort.Sort(cli.FlagsByName(app.Flags))
  var err = app.Run(os.Args)
  if err != nil {
    //log.Error(err.Error())
    return
  }
  if !ready { return }

  /* get interfaces */
  controllerCreator, ok := controller.RegisteredController[controllerProvider]
  if !ok { log.Error(fmt.Sprintf("Failed to register controller provider: %s", controllerProvider)) }
  loadbalancerCreator, ok := loadbalancer.RegisteredLoadBalancer[lbProvider]
  if !ok { log.Error(fmt.Sprintf("Failed to register loadbalancer provider: %s", lbProvider)) }

  var c = controllerCreator()
  var lb = loadbalancerCreator()
  c.New(controllerConfig)
  lb.New(lbTemplatePath, lbMapPath)
  r := make(chan route.Route, 1)

  /* read controller data and write to channel */
  log.Info(fmt.Sprintf("Reading %s controller: namespace=%s,config=%s", controllerProvider, controllerNameSpace, controllerConfig))
  go func() {
    r <- c.Publish(controllerNameSpace)
  }()

  /* receive channel data and generate lb config via Subscribe() */
  log.Info(fmt.Sprintf("Writing loadbalancer config: template=%s,map=%s", lbTemplatePath, lbMapPath))
  err = lb.Subscribe(<-r)
  if err != nil { log.Error(fmt.Sprintf("%v", err)) }

  var out = shellpipe.Local("haproxy -fD /etc/haproxy/haproxy.cfg", "")
  log.Info(fmt.Sprintf("Shell output: %s", out.SprintOut()))

  for {
    /* read controller data and write to channel */
    log.Info(fmt.Sprintf("Reading %s controller: namespace=%s,config=%s", controllerProvider, controllerNameSpace, controllerConfig))
    go func() {
      r <- c.Publish(controllerNameSpace)
    }()

    /* receive channel data and generate lb config via Subscribe() */
    log.Info(fmt.Sprintf("Writing loadbalancer config: template=%s,map=%s", lbTemplatePath, lbMapPath))
    err = lb.Subscribe(<-r)
    if err != nil { log.Error(fmt.Sprintf("%v", err)) }
    out = shellpipe.Local("uptime", "")
    log.Info(fmt.Sprintf("Shell output: %s", out.SprintOut()))

    time.Sleep(time.Second * 10)
  }
}
