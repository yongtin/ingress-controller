package route

type Route struct {
  Backends []Backend
  Error error
}

type Backend struct { // FIXME: this is haproxy specific
  BackendName string
  ListenAddress string
  ListenPort int
  BackendMode string
  LoadBalanceAlgorithm string
  HealthCheckURL string
  HostHeader string
  ServerName string
  ServerAddr string
  ServerPort int
  ServerOption string
}
