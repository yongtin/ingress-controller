package shellpipe

import (
  "os"
  "fmt"
  "time"
  "bytes"
  "math/rand"
  "os/exec"
  "io/ioutil"
)

// Local executes local commands
func Local(cmd string, input string) (out CommandOutput){
  var b bytes.Buffer
  var e bytes.Buffer
  var err error
  var execTime = time.Now()

  //cmdList = strings.Split(cmd, " ")
  //cmdSession := exec.Command(cmdList[0], cmdList[1:]...)
  var tmpFileName = fmt.Sprintf(".go-pipeline-sshpipe-local-%d", rand.Intn(10000000))
  var tmpFileAbsPath = fmt.Sprintf("/tmp/%s", tmpFileName)

  var cmdData = []byte(fmt.Sprintf(`#!/bin/bash -x
  %s
  `, cmd))
  err = ioutil.WriteFile(tmpFileAbsPath, cmdData, 0750)
  if err != nil {
    return CommandOutput{
      User: "",
      HostName: "127.0.0.1",
      ExecTime: execTime,
      Command: cmd,
      Error: fmt.Errorf("error generating run script: %v", err),
    }
  }
  cmdSession := exec.Command("sh", tmpFileAbsPath)
  cmdSession.Stdout = &b
  cmdSession.Stderr = &e
  cmdStdin, err := cmdSession.StdinPipe()
  if err != nil {
    return CommandOutput{
      User: "",
      HostName: "127.0.0.1",
      ExecTime: execTime,
      Command: cmd,
      Error: err,
    }
  }
  cmdStdin.Write([]byte(fmt.Sprintf("%s\n", input)))
  if err = cmdSession.Run(); err != nil {
    return CommandOutput{
      User: "",
      HostName: "127.0.0.1",
      ExecTime: execTime,
      Command: cmd,
      StdOut: b.String(),
      StdErr: e.String(),
      Error: err,
    }
  }

  _ = os.Remove(tmpFileAbsPath)
  /*if err != nil { // need to failed?
    return CommandOutput{
      User: "",
      HostName: "127.0.0.1",
      ExecTime: execTime,
      Command: cmd,
      StdErr: e.String(),
      Error: fmt.Errorf("error removing temporary run script: %v", err),
    }
  }*/
  return CommandOutput{
    User: "",
    HostName: "127.0.0.1",
    ExecTime: execTime,
    Command: cmd,
    StdOut: b.String(),
    StdErr: e.String(),
    Error: err,
  }
}
