package shellpipe

import "testing"
import log "gopkg.in/inconshreveable/log15.v2"

func TestLocal(t *testing.T) {
  var output = Local("uname -a")
  log.Info(output.Sprint())

  output = Local("/wtf/wtf.test")
  log.Info(output.Sprint())

  output = Local("/wtf/wtf.test very long command that does not work and should failed this test")
  log.Info(output.Sprint())

}
