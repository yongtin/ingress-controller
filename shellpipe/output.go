package shellpipe

import "fmt"
import "time"
// import https://github.com/fatih/color :)

// CommandOutput holds data structure for shellpipe.Shell output
type CommandOutput struct {
  User string
  HostName string
  ExecTime time.Time
  Command string
  StdOut string
  StdErr string
  Error error
}

// CopyOutput holds data structure for shellpipe.Copy output
type CopyOutput struct {
  User string
  HostName string
  ExecTime time.Time
  Success bool
  Error error
}

// Sprint for CopyOutput prints copy status
func (c *CopyOutput) Sprint() (result string) {
  if c.Success {
    return fmt.Sprintf(`[%s %s@%s] => {"success": true}`,  c.ExecTime.Format(time.RFC3339), c.User, c.HostName)
  }
  return fmt.Sprintf(`[%s %s@%s] => {"success": false, "err": "%v"}`, c.ExecTime.Format(time.RFC3339), c.User, c.HostName, c.Error)
}

// SprintOut only prints out stdout
func (o *CommandOutput) SprintOut() (result string) {
  return fmt.Sprintf(`[%s %s@%s] %s => {"stdout": "%s"}`, o.ExecTime.Format(time.RFC3339), o.User, o.HostName, o.Command, o.StdOut)
}

// Sprint prints out stdout stderr and err
func (o *CommandOutput) Sprint() string {
  return fmt.Sprintf(`[%s %s@%s] %s => {"stdout": "%s", "stderr": "%s", "err": "%v"}`, o.ExecTime.Format(time.RFC3339), o.User, o.HostName, o.Command, o.StdOut, o.StdErr, o.Error)
}

// SprintErr prints out stdout and then stderr
func (o *CommandOutput) SprintErr() string {
  return fmt.Sprintf(`[%s %s@%s] %s => {"stderr": "%s", "err": "%v"}`, o.ExecTime.Format(time.RFC3339), o.User, o.HostName, o.Command, o.StdErr, o.Error)
}

// Serror prints out stderr
func (o *CommandOutput) Serror() string {
  return fmt.Sprintf(`[%s %s@%s] %s => {"err": "%v"}`, o.ExecTime.Format(time.RFC3339), o.User, o.HostName, o.Command, o.Error)
}
