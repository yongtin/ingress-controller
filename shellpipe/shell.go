package shellpipe

import (
  "fmt"
  "time"
  "bytes"
  "golang.org/x/crypto/ssh"
)

// Shell remote login via ssh into remote host
func Shell(config *ssh.ClientConfig, hosts []string, cmd string) (results []CommandOutput) {
  var result = make(chan CommandOutput, len(hosts))
  for _, host := range hosts {
    go func(hostname string) {
      result <- shell(config, hostname, cmd)
    }(host)
  }

  for i:=0; i<len(hosts); i++ {
    select {
    case r := <-result:
      results = append(results, r)
    }
  }
  return results
}

func shell(config *ssh.ClientConfig, hostname string, cmd string) (_ CommandOutput) {
  var b bytes.Buffer
  var e bytes.Buffer
  var execTime = time.Now()

  session, err := sshHandshake(config, hostname, 30)
  if err != nil {
    return CommandOutput{
      User: config.User,
      HostName: hostname,
      ExecTime: execTime,
      Command: cmd,
      Error: fmt.Errorf("sshHandshake failed (cmd=%s): %v", cmd, err),
    }
  }
  session.Stdout = &b
  session.Stderr = &e
  if err := session.Run(cmd); err != nil {
    return CommandOutput{
      User: config.User,
      HostName: hostname,
      ExecTime: execTime,
      Command: cmd,
      StdOut: b.String(),
      StdErr: e.String(),
      Error: fmt.Errorf("(cmd='%s'): %v", cmd, err),
    }
  }
  defer session.Close()
  return CommandOutput{
    User: config.User,
    HostName: hostname,
    ExecTime: execTime,
    Command: cmd,
    StdOut: b.String(),
    StdErr: e.String(),
    Error: err,
  }

}
