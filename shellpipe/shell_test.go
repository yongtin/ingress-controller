package shellpipe

//import "github.com/stretchr/testify/assert"
import "testing"
//import "fmt"
//import "time"
import log "gopkg.in/inconshreveable/log15.v2"

func TestShellPipe(t *testing.T) {
  var cmd string

  cmd = "uptime"
  var outputs []CommandOutput
  outputs = Shell(
    SSHClient("vagrant", "vagrant"),
    []string{"localhost:9999", "localhost:8888"},
    cmd,
  )

  for _, output := range outputs {
    log.Info(output.Sprint())
  }

  cmd = "ls /wtf"
  outputs = Shell(
    SSHClient("vagrant", "vagrant"),
    []string{"localhost:9999", "localhost:8888"},
    cmd,
  )

  for _, output := range outputs {
    log.Info(output.Sprint())
  }

  cmd = "uptime"
  outputs = Shell(
    SSHClient("vagrant", "vagnt"),
    []string{"localhost:9999", "localhost:8888"},
    cmd,
  )

  for _, output := range outputs {
    log.Info(output.SprintOut())
    log.Info(output.Serror())
    log.Info(output.SprintErr())
  }


}
/*
  cmd = "uptime"
  stdout, stderr, err = Shell(SSHClient("vagrant", "vagrant"), "localhost:8888", cmd)
  assert.Error(t, err)
  log.Info(fmt.Sprintf("(%s) \nstdout: %s \nstderr: %s\nerr: %v", cmd, stdout, stderr, err))

  cmd = "ls /wtf"
  stdout, stderr, err = Shell(SSHClient("vagrant", "vagrant"), "localhost:9999", cmd)
  assert.Error(t, err)
  log.Info(fmt.Sprintf("(%s) \nstdout: %s \nstderr: %s\nerr: %v", cmd, stdout, stderr, err))

  cmd = "uptime"
  stdout, stderr, err = Shell(SSHClient("vagrant", "vt"), "localhost:9999", cmd)
  assert.Error(t, err)
  log.Info(fmt.Sprintf("(%s) \nstdout: %s \nstderr: %s\nerr: %v", cmd, stdout, stderr, err))

}

func TestShellPipeCopy(t *testing.T) {
  var stdout string
  var stderr string
  var err error
  var cmd string
  var copySuccess bool

  copySuccess, err = Copy(SSHClient("vagrant", "vagrant"), "localhost:9999", "/tmp/", ".sshpipe_file.test", "xxxxxxxDATAxxxxx", "root", 0555)
  assert.True(t, copySuccess)
  assert.NoError(t, err)

  cmd = "ls -lart /tmp/.sshpipe_file.test"
  stdout, stderr, err = Shell(SSHClient("vagrant", "vagrant"), "localhost:9999", cmd)
  log.Info(fmt.Sprintf("(%s) \nstdout: %s \nstderr: %s\nerr: %v", cmd, stdout, stderr, err))

  log.Info(fmt.Sprintf("Wait 5 seconds.. "))
  for i := 0; i < 5; i++ {
    time.Sleep(time.Second)
  }
  copySuccess, err = Copy(SSHClient("vagrant", "vagrant"), "localhost:9999", "/tmp/", ".sshpipe_file.test", "DATA", "vagrant", 0644)
  assert.True(t, copySuccess)
  assert.NoError(t, err)

  stdout, stderr, err = Shell(SSHClient("vagrant", "vagrant"), "localhost:9999", cmd)
  log.Info(fmt.Sprintf("(%s) \nstdout: %s \nstderr: %s\nerr: %v", cmd, stdout, stderr, err))

}*/
