package shellpipe

import (
  "net"
  "time"
  "fmt"
  "golang.org/x/crypto/ssh"
)

// SSHClient returns an ssh client config
func SSHClient(username string, password string) (config *ssh.ClientConfig) {
  var pwdAuthMethod = ssh.Password(password)
  config = &ssh.ClientConfig{
    User: username,
    Auth: []ssh.AuthMethod{
      pwdAuthMethod,
    },
  }
  return
}

func sshHandshake(config *ssh.ClientConfig, hostname string, timeout int) (session *ssh.Session, err error) {
  _, err = net.DialTimeout("tcp", hostname, time.Duration(timeout)*time.Second)
  if err != nil {
    var returnString = fmt.Sprintf("sshHandshake failed to netdial (%s): %v", hostname, err)
    return session, fmt.Errorf(returnString)
  }
  sshClient, err := ssh.Dial("tcp", hostname, config)
  if err != nil {
    var returnString = fmt.Sprintf("sshHandshake failed to sshdial (%s): %v", hostname, err)
    return session, fmt.Errorf(returnString)
  }
  session, err = sshClient.NewSession()
  if err != nil {
    var returnString = fmt.Sprintf("sshHandshake failed to create session (%s): %v", hostname, err)
    return session, fmt.Errorf(returnString)
  }
  return session, err
}
